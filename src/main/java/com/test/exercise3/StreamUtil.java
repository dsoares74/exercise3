package com.test.exercise3;

import static com.test.exercise3.text.TextUtil.isVowel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.test.exercise3.stream.Stream;
import com.test.exercise3.stream.StreamException;

public final class StreamUtil {

	private StreamUtil() {
		super();
	}

	public static char firstChar(final Stream input) {
		final Map<Character, Integer> counts = new LinkedHashMap<Character, Integer>();
		final List<Character> chars = new ArrayList<>();

		while (input.hasNext()) {
			final char c = input.getNext();
			if (isVowel(c)) {
				counts.merge(c, 1, Integer::sum);
			}

			chars.add(c);
		}

		final Optional<Character> firstVowel = counts.entrySet().stream()
				.filter(e -> e.getValue() == 1 && checkPreviouses(chars, e.getKey())).map(e -> e.getKey()).findFirst();
		return firstVowel.orElseThrow(() -> new StreamException("Did not find any non repeated vowel"));
	}

	private static boolean checkPreviouses(final List<Character> chars, final char c) {
		final int index = chars.indexOf(c);
		return index >= 2 && isVowel(chars.get(index - 2)) && !isVowel(chars.get(index - 1));
	}
}