package com.test.exercise3.stream;

public class StreamException extends RuntimeException {

	private static final long serialVersionUID = 6228153892935042882L;

	public StreamException() {
		super();
	}

	public StreamException(final String message) {
		super(message);
	}

	public StreamException(final Throwable cause) {
		super(cause);
	}

	public StreamException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
