package com.test.exercise3.stream;

public class StreamImpl implements Stream {

	private final String text;
	private int count = 0;

	public StreamImpl(final String text) {
		this.text = text;
	}

	@Override
	public char getNext() {
		return hasNext() ? text.charAt(count++) : text.charAt(text.length() - 1);
	}

	@Override
	public boolean hasNext() {
		return count < text.length();
	}

}
