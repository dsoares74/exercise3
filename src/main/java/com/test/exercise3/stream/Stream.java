package com.test.exercise3.stream;

public interface Stream {

	char getNext();

	boolean hasNext();
}
