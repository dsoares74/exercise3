package com.test.exercise3;

import java.util.Scanner;

import com.test.exercise3.stream.StreamImpl;

public class App {
	public static void main(final String[] args) {
		final Scanner input = new Scanner(System.in);
		System.out.println("Digit a text to find the first non-repeated vowel:");
		final String text = input.nextLine();
		try {
			System.out.println("The first non-repeated vowel is " + StreamUtil.firstChar(new StreamImpl(text)));
		} catch (final Exception e) {
			System.out.println(e.getMessage());
		}
		input.close();
	}
}
