package com.test.exercise3.text;

import static java.text.Normalizer.normalize;
import static java.text.Normalizer.Form.NFD;
import static java.util.regex.Pattern.compile;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;

public final class TextUtil {

	private static final String EMPTY_STRING = "";
	private static final Pattern VOWEL_PATTERN = compile("[aeiou]", Pattern.CASE_INSENSITIVE);
	private static final Pattern DIACRITICAL_PATTERN = compile("\\p{InCombiningDiacriticalMarks}+");

	private TextUtil() {
		super();
	}

	public static boolean isVowel(final Character c) {
		return VOWEL_PATTERN.matcher(stripAccents(c.toString())).matches();
	}

	public static String stripAccents(final String input) {
		final Function<String, String> stripDiacriticalMarks = s -> DIACRITICAL_PATTERN.matcher(normalize(s, NFD)).replaceAll(EMPTY_STRING);
		return Optional.ofNullable(input).map(stripDiacriticalMarks).orElse(null);
	}

}
