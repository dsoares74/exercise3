package com.test.exercise3.text;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.stream.Stream;

import org.junit.Test;

public class TextUtilTest {

	@Test
	public final void testIsVowel_WhenCharIsVowel_shouldReturnTrue() {
		final Stream<Character> vowels = Stream.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
		assertTrue(vowels.allMatch(TextUtil::isVowel));
	}

	@Test
	public final void testIsVowel_WhenCharIsConsonant_shouldReturnFalse() {
		final Stream<Character> consonants = Stream.of('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z');
		assertTrue(consonants.noneMatch(TextUtil::isVowel));
	}

	@Test
	public final void testStripAccents() {
		final String source = "ń  ǹ  ň  ñ  ṅ  ņ  ṇ  ṋ  ṉ  Ñ Á â å ä À ã é Ê è ë è Í î ì Ï Ó ô ò õ ö ú Û ù Ü Ç Ý";
		final String result = "n  n  n  n  n  n  n  n  n  N A a a a A a e E e e e I i i I O o o o o u U u U C Y";
		assertEquals(result, TextUtil.stripAccents(source));
	}

}
