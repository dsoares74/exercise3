package com.test.exercise3.stream;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.test.exercise3.StreamUtil;
import com.test.exercise3.stream.Stream;
import com.test.exercise3.stream.StreamException;
import com.test.exercise3.stream.StreamImpl;

public class StreamUtilTest {

	private Stream stream;

	@Test(expected = StreamException.class)
	public final void testFirstChar_whenTextOnlyRepeatedVowel_shouldThrowException() {
		stream = new StreamImpl("AhvartaerjeA");
		StreamUtil.firstChar(stream);
	}

	@Test(expected = StreamException.class)
	public final void testFirstChar_whenTextOnlyConsonants_shouldThrowException() {
		stream = new StreamImpl("qwrRTykhSzxmn");
		StreamUtil.firstChar(stream);
	}

	@Test
	public final void testFirstChar_whenTextHasNonRepeatedVowel_matchPreConditions_shouldReturnSuccess() {
		stream = new StreamImpl("cAAbntfuKilmfor");
		assertEquals('i', StreamUtil.firstChar(stream));
	}

	@Test(expected = StreamException.class)
	public final void testFirstChar_whenTextHasNonRepeatedVowel_noMatchPreConditions_shouldReturnSuccess() {
		stream = new StreamImpl("Aeiou");
		StreamUtil.firstChar(stream);
	}

}
