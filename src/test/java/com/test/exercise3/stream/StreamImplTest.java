package com.test.exercise3.stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.test.exercise3.stream.Stream;
import com.test.exercise3.stream.StreamImpl;

public class StreamImplTest {

	private Stream stream;

	@Before
	public void setUp() throws Exception {
		stream = new StreamImpl("A");
	}

	@Test
	public final void testGetNext() {
		assertEquals('A', stream.getNext());
	}

	@Test
	public final void testHasNext() {
		assertTrue(stream.hasNext());
	}

	@Test
	public final void testHasNext_whenNoMoreChar_shouldReturnFalse() {
		stream.getNext();
		assertFalse(stream.hasNext());
	}

}
