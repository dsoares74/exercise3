# EXERCISE3 #

* Dada uma stream, encontre o primeiro caracter Vogal, após uma consoante, onde a mesma é antecessora a uma vogal e que não se repita no resto da stream. 
* O termino da leitura da stream deve ser  garantido através do método hasNext(), ou seja, retorna falso para o termino da leitura da stream.
* Exemplo:
* Input:  aAbBABacafe
* Output: e
* No exemplo, ‘e’ é o primeiro caracter Vogal da stream que não se repete após a primeira Consoante ‘f’o qual tem uma vogal ‘a’ como antecessora

# Unit Tests

* We are using JUnit for unit tests.

* To run, execute the following steps in terminal:

```sh
$ mvn test
```

### How do I get set up? ###

* Dependencies
To get the Dependencies, simply run the following command:

```sh
$ mvn clean package
```

To start the application, simply run the following command:

```sh
$ java -jar target/exercise3-0.0.1-SNAPSHOT.jar
```


